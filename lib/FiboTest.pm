package FiboTest;

use File::Copy;
use File::Path qw{ mkpath rmtree };
use FindBin    qw{ $Bin };
use Test::More tests => 1;

use parent qw{ Exporter };
our @EXPORT = qw{ fibo_check };

# fibo_check( $lang, $ext, $cmd )
sub fibo_check {
    my ($lang, $ext, $cmd) = @_;

    # clean room for the test
    my $dir = "$Bin/tmp/$lang";
    rmtree($dir);
    mkpath($dir);

    # get expected result
    my $want = do { local $/; <DATA> }; 

    # run the test
    copy( "$Bin/../fibonacci.txt", "$dir/fibonacci.$ext");
    my $command = sprintf "cd $dir; $cmd 2>&1", "fibonacci.$ext";
    my $have = qx{ $command };

    is( $have, $want, "language $lang test");

    # clean after ourselve
    rmtree($dir) unless $ENV{POLYGLOT_DEBUG};
}

1;

__DATA__
1
1
2
3
5
8
13
21
34
55
